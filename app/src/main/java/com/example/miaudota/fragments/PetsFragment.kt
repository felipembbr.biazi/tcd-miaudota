package com.example.miaudota.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.miaudota.R
import com.example.miaudota.activitys.RecyclerAdapter
import com.example.miaudota.api.RetrofitClient
import com.example.miaudota.api.models.Animai
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"


/**
 * A simple [Fragment] subclass.
 * Use the [PetsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PetsFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var recyclerView: RecyclerView
    private lateinit var list:List<Animai>
    lateinit var recyclerAdapter: RecyclerAdapter
    //lateinit var linearLayoutManager: LinearLayoutManager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        val linearLayoutManager = LinearLayoutManager(requireContext())

        list = ArrayList()
        recyclerView.layoutManager = linearLayoutManager

        //val adapter = RecyclerAdapter(list,this.requireContext())

        RetrofitClient.instance.getAnimal()
            .enqueue(object: Callback<Animai>{
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(call: Call<Animai>, response: Response<Animai>) {
                    /*if(response.isSuccessful){
                        (list as ArrayList<Animai>).clear()
                        for (myData in response.body()){
                            (list as ArrayList<Animai>).add(myData)
                        }
                        adapter.notifyDataSetChanged()
                        recyclerView.adapter = adapter
                    }*/
                }
                override fun onFailure(call: Call<Animai>, t: Throwable) {
                    Toast.makeText(requireContext(), t.message, Toast.LENGTH_LONG).show()
                }

            } )


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_pets, container, false)
        this.recyclerView = v.findViewById(R.id.frag_recycler_view)
        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PetsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PetsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }




}