package com.example.miaudota.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.miaudota.R
import com.example.miaudota.activitys.MainActivity
import com.example.miaudota.api.models.Usuario
import com.example.miaudota.storage.SharedPrefManager

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PerfilFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PerfilFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var bt_test: Button
    private lateinit var imgView: ImageView
    private lateinit var txtPerfil: TextView
    private lateinit var txtEMAIL: TextView
    private lateinit var txtEndereco: TextView
    private lateinit var txtTelefone: TextView

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

        val ref = this.context?.getSharedPreferences("my_shared_preff", Context.MODE_PRIVATE)
        val name = ref?.getString("nome", null)
        val email = ref?.getString("email", null)
        val endereco = ref?.getString("endereco", null)
        val telefone = ref?.getString("telefone", null)

        this.txtPerfil.setText(name)
        this.txtEMAIL.setText(email)
        this.txtEndereco.setText(endereco)
        this.txtTelefone.setText(telefone)

        val url = ref?.getString("avatar", null)
        if(url != null) {
            Glide.with(requireContext()).load(url)
                .apply(RequestOptions().override(700,550))
                .into(this.imgView)
         }

        this.bt_test.setOnClickListener {
            SharedPrefManager.getInstance(requireContext()).clear()
            requireActivity().finish()
            val intent = Intent(requireActivity(),MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
        }

    }


    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_perfil, container, false)
        this.imgView = v.findViewById(R.id.imgView)
        this.bt_test = v.findViewById(R.id.bt_test)
        this.txtPerfil = v.findViewById(R.id.txtPerfil)
        this.txtEMAIL = v.findViewById(R.id.txtEMAIL)
        this.txtEndereco = v.findViewById(R.id.txtEndereco)
        this.txtTelefone = v.findViewById(R.id.txtTelefone)

        return v
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PerfilFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PerfilFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }


}