package com.example.miaudota.api.models

data class Usuario(
    val ativo: Boolean,
    val avatar: String? = null,
    val created_at: String,
    val email: String,
    val endereco: String,
    val id: Int,
    val nome: String,
    val senha: String,
    val telefone: String,
    val tipo: String? = null,
    val updated_at: Any
)