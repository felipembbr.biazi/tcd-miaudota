package com.example.miaudota.api.models

data class Register(
    val sucesso: Boolean,
    val mensagem: String
)