package com.example.miaudota.api

import com.example.miaudota.api.models.Animai
import com.example.miaudota.api.models.Autenticacao
import com.example.miaudota.api.models.BuscaAnimais
import com.example.miaudota.api.models.Register
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @FormUrlEncoded
    @POST("/Autenticacao/Register")
    fun register(
        @Field("email") email: String,
        @Field("tipo") tipo: String,
        @Field("nome") nome: String,
        @Field("senha") senha: String,
        @Field("endereco") endereco: String,
        @Field("telefone") telefone: String
    ): Call<Register>

    @FormUrlEncoded
    @POST("/Autenticacao/")
    fun login(@Field("email") email: String,@Field("senha") senha: String ): Call<Autenticacao>


    @GET("/Animais/")
    fun getAnimal():Call<Animai>
}