package com.example.miaudota.api.models

data class Autenticacao(
    val sucesso: Boolean,
    val token: String,
    val usuario: Usuario,
    val mensagem: String
)