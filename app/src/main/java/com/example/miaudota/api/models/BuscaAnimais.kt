package com.example.miaudota.api.models

data class BuscaAnimais(
    val animais: List<Animai>,
    val sucesso: Boolean
)
