package com.example.miaudota.api

import android.media.session.MediaSession.Token
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import java.util.concurrent.TimeUnit

object RetrofitClient {
    private const val BASE_URL = "http://192.168.15.26:3000"

    var token: String = "";

    fun Token(token: String ) {
        this.token = token;
    }

    private val client = OkHttpClient.Builder()
        .connectTimeout(10,TimeUnit.SECONDS)
        .readTimeout(10, TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        })
        .addInterceptor {
            val request = it.request()

            if(!token.isNullOrEmpty())
                return@addInterceptor it.proceed(request)

            val newRequest = request.newBuilder()
                .addHeader("Authorization", "Bearer" + token)
                .build()
            it.proceed(newRequest)
        }
        .build()

    fun gson(): Gson = GsonBuilder().create()

    val instance: ApiService by lazy {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson()))
                .build()

        retrofit.create(ApiService::class.java)
    }
}

