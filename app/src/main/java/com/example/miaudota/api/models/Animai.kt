package com.example.miaudota.api.models

data class Animai(
    val avatar: Any,
    val created_at: String,
    val descricao: String,
    val id: Int,
    val id_usuario: Int,
    val idade: Int,
    val nome: String,
    val sexo: String,
    val status: String,
    val updated_at: Any
)