package com.example.miaudota.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
//import com.example.miaudota.PetAdapter
import com.example.miaudota.R
//import com.example.miaudota.model.fakePets
import android.util.Log

import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.miaudota.api.RetrofitClient
import com.example.miaudota.api.models.Autenticacao
import com.example.miaudota.storage.SharedPrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    private lateinit var textRegistro: TextView
    private lateinit var etxtEmail: EditText
    private lateinit var etxtSenha: EditText
    private lateinit var btLogin: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        this.etxtEmail = findViewById(R.id.etxtEmail)
        this.etxtSenha = findViewById(R.id.etxtSenha)
        this.textRegistro = findViewById(R.id.textRegistro)
        this.btLogin = findViewById(R.id.btLogin)

        this.textRegistro.setOnClickListener {
            startActivity(Intent(this@MainActivity, RegisterActivity::class.java))
        }

        this.btLogin.setOnClickListener {

            val email = this.etxtEmail.text.toString()
            val senha = this.etxtSenha.text.toString()

            if (email.isEmpty()) {
                this.etxtEmail.error = "O email deve ser informado"
                this.etxtEmail.requestFocus()
                return@setOnClickListener
            }

            if (senha.isEmpty()) {
                this.etxtSenha.error = "A senha deve ser informada"
                this.etxtSenha.requestFocus()
                return@setOnClickListener
            }

            RetrofitClient.instance.login(email = email, senha = senha)
                .enqueue(object : Callback<Autenticacao> {
                    override fun onResponse(
                        call: Call<Autenticacao>,
                        response: Response<Autenticacao>
                    ) {
                        if (response.body()?.sucesso == true) {
                            Log.d("TEST123", "onResponse:${response.body().toString()} ")

                            SharedPrefManager.getInstance(applicationContext).saveUsuario(response.body()?.usuario!!)
                            val intent = Intent(applicationContext, MenuActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

                            startActivity(intent)

                        } else if (response.body() == null){
                           Toast.makeText(applicationContext,"Erro: Favor verificar se Login e/ou Senha está correto", Toast.LENGTH_LONG).show()
                        }
                    }


                    override fun onFailure(call: Call<Autenticacao>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                    }

                })
        }

    }

    override fun onStart() {
        super.onStart()

        if(SharedPrefManager.getInstance(this).isLoggedIn) {
            val intent = Intent(applicationContext, MenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            startActivity(intent)
        }
    }
}

