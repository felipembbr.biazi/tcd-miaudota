package com.example.miaudota.activitys

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.miaudota.R
import com.example.miaudota.fragments.HomeFragment
import com.example.miaudota.fragments.PerfilFragment
import com.example.miaudota.fragments.PetsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MenuActivity : AppCompatActivity() {
    private val petsFragment = PetsFragment()
    private val homeFragment = HomeFragment()
    private val perfilFragment = PerfilFragment()
    private lateinit var botton_navigation: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        this.botton_navigation = findViewById(R.id.botton_navigation)

        replaceFragment(petsFragment)

        botton_navigation?.setOnItemSelectedListener {
            when(it.itemId){
                R.id.ic_home -> replaceFragment(homeFragment)
                R.id.ic_perfil -> replaceFragment(perfilFragment)
                R.id.ic_pets -> replaceFragment(petsFragment)
            }
            return@setOnItemSelectedListener true
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        if (fragment != null) {
            val transicao = supportFragmentManager.beginTransaction()
            transicao.replace(R.id.fragment_container, fragment)
            transicao.commit()
        }
    }
}