package com.example.miaudota.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.LinearLayoutManager
import android.os.Bundle
//import com.example.miaudota.PetAdapter
import com.example.miaudota.R
import com.example.miaudota.fragments.PetsFragment
//import com.example.miaudota.model.fakePets
import com.example.miaudota.storage.SharedPrefManager


class RecycleActivity: AppCompatActivity() {
    private lateinit var recyclerView: RecyclerView
    private var layoutManager: RecyclerView.LayoutManager? = null
    //private var adapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>? = null
    override fun onCreate(savedInstanceState: Bundle?) {

        replaceFragment(PetsFragment())



        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_pets)
        /*
        //layoutManager = LinearLayoutManager(this)
        this.recyclerView = findViewById(R.id.recyclerView)
        //recyclerView.adapter = PetAdapter(fakePets())

        this.recyclerView.layoutManager = LinearLayoutManager(this)
        //val adapter = RecyclerAdapter()
        //this.recyclerView.adapter = adapter
        */

    }

    private fun replaceFragment(petsFragment: PetsFragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, petsFragment)
        fragmentTransaction.commit()
    }


    override fun onStart() {
        super.onStart()

        if(!SharedPrefManager.getInstance(this).isLoggedIn) {
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            startActivity(intent)
        }
    }
}