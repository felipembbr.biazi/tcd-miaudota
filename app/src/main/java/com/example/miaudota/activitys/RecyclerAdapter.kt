package com.example.miaudota.activitys

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.miaudota.R
import com.example.miaudota.api.models.Animai
import com.google.android.material.imageview.ShapeableImageView


class RecyclerAdapter(val list: ArrayList<Animai>, val context: Context): RecyclerView.Adapter<RecyclerAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val item_Image : ShapeableImageView = itemView.findViewById(R.id.item_Image)
        val item_Title : TextView = itemView.findViewById(R.id.item_Title)
        val txtStatus2 : TextView = itemView.findViewById(R.id.txtStatus2)
        val item_Detail: TextView = itemView.findViewById(R.id.item_Detail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_pets, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.txtStatus2.text = list[position].status
        holder.item_Title.text = list[position].nome
        holder.item_Detail.text = list[position].descricao
        val url = list[position].avatar
        Glide.with(holder.item_Image.context).load(url).apply(RequestOptions().override(700,550)).into(holder.item_Image)
    }

    override fun getItemCount(): Int {
        return list.size
    }

}