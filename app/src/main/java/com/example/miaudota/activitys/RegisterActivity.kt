package com.example.miaudota.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.example.miaudota.R
import com.example.miaudota.api.RetrofitClient
import com.example.miaudota.api.models.Register
import com.example.miaudota.storage.SharedPrefManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {
    private lateinit var etxtEmail: EditText
    private lateinit var etxtEmail2: EditText
    private lateinit var etxtNome: EditText
    private lateinit var etxtEndereco: EditText
    private lateinit var etxtTelefone: EditText
    private lateinit var btRegister: Button
    private lateinit var textLogin: TextView
    private lateinit var switchTipe: Switch
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        this.etxtEmail = findViewById(R.id.etxtEmail)
        this.etxtEmail2 = findViewById(R.id.etxtEmail2)
        this.etxtNome = findViewById(R.id.etxtNome)
        this.etxtEndereco = findViewById(R.id.etxtEndereco)
        this.etxtTelefone = findViewById(R.id.etxtTelefone)
        this.btRegister = findViewById(R.id.btRegister)
        this.textLogin = findViewById(R.id.textLogin)
        this.switchTipe = findViewById(R.id.switchTipe)

        this.textLogin.setOnClickListener{
            startActivity(Intent(this@RegisterActivity,MainActivity::class.java))
        }

        this.btRegister.setOnClickListener {

            val email = this.etxtEmail.text.toString();
            val senha = this.etxtEmail2.text.toString();
            val nome = this.etxtNome.text.toString();
            val endereco = this.etxtEndereco.text.toString();
            val telefone = this.etxtTelefone.text.toString();

            if(email.isEmpty()) {
                this.etxtEmail.error="O email deve ser informado"
                this.etxtEmail.requestFocus()
                return@setOnClickListener
            }

            if(senha.isEmpty()) {
                this.etxtEmail2.error="A senha deve ser informada"
                this.etxtEmail2.requestFocus()
                return@setOnClickListener
            }

            if(nome.isEmpty()) {
                this.etxtNome.error="O Nome deve ser informado"
                this.etxtNome.requestFocus()
                return@setOnClickListener
            }

            if(endereco.isEmpty()) {
                this.etxtEndereco.error="Endereço deve ser informado"
                this.etxtEndereco.requestFocus()
                return@setOnClickListener
            }

            if(telefone.isEmpty()) {
                this.etxtTelefone.error="O Telefone deve ser informado"
                this.etxtTelefone.requestFocus()
                return@setOnClickListener
            }

            var tipo = this.switchTipe.text.toString()
            if (switchTipe.isChecked)
            {
                tipo = "pessoa"
            }else{
                tipo = "instituicao"
            }

            RetrofitClient.instance.register(
                email = email,
                senha = senha,
                nome = nome,
                endereco = endereco,
                telefone = telefone,
                tipo = tipo
            )
                .enqueue(object: Callback<Register>{
                    override fun onResponse(call: Call<Register>, response: Response<Register>) {
                        if (response.isSuccessful){
                            Toast.makeText(applicationContext, response.body()?.mensagem, Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onFailure(call: Call<Register>, t: Throwable) {
                        Toast.makeText(applicationContext, t.message, Toast.LENGTH_LONG).show()
                    }

                })
        }


    }

    override fun onStart() {
        super.onStart()

        if(SharedPrefManager.getInstance(this).isLoggedIn) {

            val intent = Intent(applicationContext, MenuActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

            startActivity(intent)
        }
    }
}