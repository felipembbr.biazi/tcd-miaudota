package com.example.miaudota.storage

import android.content.Context
import com.example.miaudota.api.models.Autenticacao
import com.example.miaudota.api.models.Usuario

class SharedPrefManager private constructor(private val mCtx: Context) {

    val isLoggedIn: Boolean
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return sharedPreferences.getInt("id", -1) != -1
        }

    val usuario: Usuario
        get() {
            val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
            return Usuario(
                sharedPreferences.getBoolean("ativo", true),
                sharedPreferences.getString("avatar", null),
                sharedPreferences.getString("created_at", "")!!,
                sharedPreferences.getString("email", "")!!,
                sharedPreferences.getString("endereco", "")!!,
                sharedPreferences.getInt("id",-1),
                sharedPreferences.getString("nome", "")!!,
                sharedPreferences.getString("senha", "")!!,
                sharedPreferences.getString("telefone", "")!!,
                sharedPreferences.getString("tipo", "")!!,
                sharedPreferences.getString("updated_at" , "")!!
            )
        }


    fun saveUsuario(usuario: Usuario) {

        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()

        editor.putInt("id", usuario.id)
        editor.putString("email", usuario.email)
        editor.putString("nome", usuario.nome)
        editor.putString("endereco", usuario.endereco)
        editor.putString("telefone", usuario.telefone)
        editor.putString("avatar", usuario.avatar)

        editor.apply()

    }

    fun clear() {
        val sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    companion object {
        private val SHARED_PREF_NAME = "my_shared_preff"
        private var mInstance: SharedPrefManager? = null
        @Synchronized
        fun getInstance(mCtx: Context): SharedPrefManager {
            if (mInstance == null) {
                mInstance = SharedPrefManager(mCtx)
            }
            return mInstance as SharedPrefManager
        }
    }

}
